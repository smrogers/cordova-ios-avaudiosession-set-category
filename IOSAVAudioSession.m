 #import "IOSAVAudioSession.h"
#import <Cordova/CDV.h>

@implementation IOSAVAudioSession

	- (void)setCategory:(CDVInvokedUrlCommand*)command {
		[self.commandDelegate runInBackground:^{
			NSString* category = [command.arguments objectAtIndex:0];
			[[AVAudioSession sharedInstance] setCategory:category error:nil];

			// report success
			CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
			[self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
	}

	/*
	AVAudioSession category options
		AVAudioSessionCategoryAmbient
		//  Use this category for background sounds such as rain, car engine noise, etc. Mixes with other music

		AVAudioSessionCategorySoloAmbient
		// Use this category for background sounds. Other music will stop playing.

		AVAudioSessionCategoryPlayback
		// Use this category for music tracks.

		AVAudioSessionCategoryRecord
		//  Use this category when recording audio.

		AVAudioSessionCategoryPlayAndRecord
		//  Use this category when recording and playing back audio

		AVAudioSessionCategoryAudioProcessing
		//  Use this category when using a hardware codec or signal processor while not playing or recording audio.
	*/
@end
