#import <Cordova/CDV.h>
#import <AVFoundation/AVFoundation.h>

@interface IOSAVAudioSession : CDVPlugin

- (void)setCategory:(CDVInvokedUrlCommand*)command;

@end
